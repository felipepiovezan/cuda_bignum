# CUDA BIGNUM PROJECT

This projects aims to implement the number theoretic FFT transformation in
order to multiply big numbers. The idea can be broken down as follows:

1. Convert a big number into a polynomial representation with a certain base
   2^b.
2. Given two big numbers in such form, perform polynomial multiplication.
3. Convert the resulting polynomial to the original representation.

We indirectly use polynomial representation daily. Consider the number 1234, it
can be expressed by p(x) = 1 * x^3 + 2 * x^2 + 3 * x^1 + 4 * x^0 when x = 10
(this is the decimal base). By multiplying two such polynomials and evaluating
the result at the base, we get the resulting number multiplication.

Step 2 is tipically performed using FFT algorithms. Plenty of FFT
implementations are available, even in the CUDA standard libraries. Hoewever,
FFT is usually defined over the Complex ring of numbers, which requires
floating point representation. While the precision errors incurred by such
representation are tolerable in most applications, for integer multiplication
we required something else. 

It turns out FFTs can be performed over any ring. This project implements FFT
over Z{_p}^*, where p is a Proth prime (p = k*2^n + 1). This is not the most
efficient algorithm in terms of complexity and it has severe limitations when
it comes to overflows. 

Consider these two facts:
1. Using 64 bit registers, only base 2^10 can be used. For 32 bit registers,
   base 2^2 is used. 
2. The size 2^L of the polynomial must be at most p (minus a few constants)

This quickly limits us on the memory side of things. If we have 16GB of memory
and 64 bit registers, that's 16*2^30*2^8 bits / (2 ^ 6 bits / word) = 2^36
words. On base 2^10, the biggest number we can represent has 2^26 bits.

A more detailed discussion can be found in the excellent paper by Christoph
Luders (https://arxiv.org/abs/1503.04955).

# Compiling the project:

There are two prerequisites in order to be able to compile the project.

1. GTest library for the unit tests
2. CUDA tools

The setup for CUDA is extremely system dependent and we avoid discussion of it
here.  Just bear in mind that CMake has less than ideal support for it, and
some ugly cheats had to be applied in the CMake files.

# Compile GTEST:

In order to run the tests, the google unit test library must be compiled. It is
extremely lightweight and compilation should be very straightforward, but the
steps are detailed below:

Move to a directory where you want to work and follow these steps.

* mkdir gtest
* cd gtest
* git clone https://github.com/google/googletest.git
* mkdir build
* cd build
* mkdir install
* cmake ../ -DCMAKE\_INSTALL\_PREFIX=install/ ../googletest
* make -j4
* make install

# Compile the project
Before the compilation itself, to the /test directory and run the
huge\_number\_generator.py script. This will generate the appropriate numbers
for some of the test cases.  

Move to the directory where this project is located (but not inside this
project's directory).

* mkdir build
* cd build
* cmake -DGTEST\_ROOT=path-to-installed-gtest-dir ../bignum

where path-to-installed-gtest-dir is the "install" folder created in the GTEST
installation guide.
