#ifndef _ARITHMETIC_H
#define _ARITHMETIC_H

#include "bignum.hpp"
#include "utils.hpp"


namespace
{
	constexpr uint64_t one = 1;
	/* in utils.hpp:	
	struct proth
	{
		Digit N;
		Digit p;
		Digit k;
		Digit g;
		Digit g_inv;
	};
	*/

	/*	@brief		Helper functions that return the appropriate prime
	 *				based on the type used (32/64 bits)
	 */
	constexpr utils::proth<uint32_t> proth32 = {30, 3221225473, 3, 5, 1932735284}; //2^31.58
	constexpr utils::proth<uint64_t> proth64 = {57, 4179340454199820289, 29, 3, 1393113484733273430}; //2^61.85
	template<typename DigitType> utils::proth<DigitType> get_proth();
	template<> utils::proth<uint32_t> get_proth<uint32_t>() {return proth32;}
	template<> utils::proth<uint64_t> get_proth<uint64_t>() {return proth64;}
	

	/* @brief			Helper function that returns an appropriate proth prime
	 * 					that will be used by the multiplication algorithm.
	 * @precondition	digits_count must be a power of 2.
	 * @args			digits_count	is usually the size returned by product_size().
	 */
	template<typename DigitType, std::size_t log_base>
	utils::proth<DigitType> get_multiplication_fft_proth(std::size_t digits_count)
	{
		assert(utils::log2(digits_count).second == true);

		auto proth = get_proth<DigitType>();
		auto logp = utils::log2(proth.p).first + (utils::log2(proth.p).second == false);
		auto logn = utils::log2(digits_count).first;
		if (logp < logn + 2 * log_base - 1)
		{
			throw "There is no prime big enough that fits in a Digit";
		}

		constexpr std::size_t word_size = 8 * sizeof(DigitType);
		
		if (logn > word_size - 2*log_base)
		{
			throw "Too many digits in the container";
		}

		//adjust k so that proth.n == logn
		if (proth.N > logn)
		{
			DigitType one = 1;
			auto pow_diff = proth.N - logn;
			proth.k = proth.k * (one << pow_diff);
			proth.N -= pow_diff;
		}
		assert(proth.N == logn);

		return proth;
	}
}

namespace bn
{
	/*
	 * @brief		Returns the number of digits required by
	 * 				the resulting polynomial multiplication of
	 * 				a and b.
	 *
	 * @details		Given two polynomials a and b, of degree d(a) and d(b),
	 *				their product has degree at most d(a) + d(b).  However, we will work
	 *				with degree bounds, since they work better with vector lengths. Their
	 *				degree bound is len(a) and len(b). Their product has bound len(a) +
	 *				len(b) - 1. Eg: p(x) = x^2 + 2x + 0 has degree 2 and degree bound 3.
	 *				Notice that 3 is the number of positions its vector would have, aka 
	 *				(1, 2, 0).
	 * @return 		The the number of digits required for the resulting polynomial c = a * b.
	 */
	template <typename T, uint32_t log_base>
	uint64_t product_size(const bignum<T, log_base>& a, const bignum<T, log_base>& b)
	{
		auto size_a = a.num_digits();
		auto size_b = b.num_digits();
		auto mul_base_size = size_a + size_b;
		auto log = utils::log2(mul_base_size);
		uint64_t N = log.first;
		if (!log.second)
			N++;
		return one << N;
	}

	/*
	 * @brief		Return a bignum equivalento to a*b
	 * @details		Implements the FFT-based polynomial multiplication
	 * 				of numbers. For a detailed discussion, see section 2.8
	 * 				of: http://arxiv.org/abs/1503.04955
	 * @args		a, b	The numbers to be multiplied
	 */
	template <typename T, uint32_t log_base>
	bignum<T, log_base> bn_mult(const bignum<T, log_base>& a_, const bignum<T, log_base>& b_)
	{
		auto size = product_size(a_, b_);
		auto a = a_;
		auto b = b_;
		a.pad_to(size);
		b.pad_to(size);
		auto proth = get_multiplication_fft_proth<T, log_base>(size);

		auto p1 = utils::in_place_discrete_fft<
			typename bignum<T, log_base>::digit, 
			typename bignum<T, log_base>::ddigit,
			log_base,
			typename bignum<T, log_base>::digit_container>
			(a.digits_, proth);
		auto p2 = utils::in_place_discrete_fft<
			typename bignum<T, log_base>::digit, 
			typename bignum<T, log_base>::ddigit,
			log_base,
			typename bignum<T, log_base>::digit_container>
			(b.digits_, proth);
		assert(p1 == p2);

		for (std::size_t i = 0; i < size; ++i)
		{
			typename bignum<T, log_base>::ddigit x = a.digits_[i];
			x = (x * b.digits_[i]) % p1;
			a.digits_[i] = x;
		}

		utils::in_place_discrete_inverse_fft<
			typename bignum<T, log_base>::digit, 
			typename bignum<T, log_base>::ddigit,
			log_base,
			typename bignum<T, log_base>::digit_container>
			(a.digits_, proth);

		constexpr typename bignum<T, log_base>::digit one = 1;
		constexpr typename bignum<T, log_base>::digit base = one << log_base;
		for(std::size_t i = 0; i < a.digits_.size() - 1; i++)
		{
			assert(a.digits_[i + 1] + a.digits_[i] / base >= a.digits_[i+1]);
			a.digits_[i + 1] += a.digits_[i] / base;
			a.digits_[i] = a.digits_[i] % base;
		}
		assert(a.digits_[a.digits_.size() - 1] / base == 0);

		return a;
	}
	
}

#endif
