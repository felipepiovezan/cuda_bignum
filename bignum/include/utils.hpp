#ifndef _UTILS_H
#define _UTILS_H

#include <cassert>
#include <cstdint>
#include <utility>
#include "proth_primes.hpp"
#include <vector>
#include <iostream>

namespace
{
	constexpr uint64_t ONE = 1;
}

namespace utils
{
	/* @brief		Compute the log base 2 of a number.
	 * @returns		A pair <log, exact> where log is the
	 * 				integral portion of the logarithm and
	 * 				exact is a boolean indicating whether
	 * 				it was an exact logarithm or not.
	 * @remarks		If x is 0, this function has undefined behavior
	 */
	template <typename Digit>
	std::pair<uint32_t, bool> log2(Digit x)
	{
		int32_t log;
		for(log = sizeof(Digit)*8 -1; log >= 0; log--)
		{
			auto digit = (x >> log) && 1;
			if (digit)
			{
				break;
			}
		}

		bool exact = (x ^ (1 << log)) == 0;

		return std::make_pair(log, exact);
	}

	/* @brief 		Compute the bit reversal of a value.
	 * @remarks		Base on the implementation found at:
	 * 				www.graphics.stanford.edu/~seander/bithacks.html
	 * @args		k		The value to bit reversed.
	 * @args		bits	The number of least significant bits 
	 * 						from k that are to be considered. This 
	 * 						value is assumed to be strictly greater
	 * 						than 0. All other bits become zero.
	 * @example		rev(010110, 3) = 000011
	 * @example		rev(010110, 6) = 011010
	 */
	template <typename digit>
	digit rev(digit k, uint8_t bits = 8*sizeof(digit))
	{
		auto reversed = k & 1;
		bits--;

		for(k >>= 1; bits; k >>= 1)
		{
			reversed <<= 1;
			reversed |= k & 1;
			bits--;
		}
		
		reversed <<= bits;
		
		return reversed;
	}

	/*
	 * @brief		For each position i of a given container,
	 * 				swap the elements i and rev(i).
	 * @remarks		This assumes the container has length equal to
	 * 				a power of 2.
	 * @params		container		The container where the swaps are
	 * 								to be performed in place.
	 */
	template <typename C>
	void in_place_bit_reverse_swap(C &container)
	{
		auto bits = log2(container.size()).first;
		assert(log2(container.size()).second == true);

		for(uint64_t i = 0; i < container.size(); ++i)
		{
			auto pos = rev(i, bits);
			if (pos > i)
			{
				std::swap(container[i], container[pos]);
			}
		}
	}

	/*
	 * @brief		Computes base^exponent modulo mod
	 */
	template <typename Digit, typename DoubleDigit = Digit>
	Digit modular_exp(Digit base, Digit exponent, Digit mod)
	{
		DoubleDigit ans = 1;
		for(int i = sizeof(Digit)*8 - 1 ; i >= 0; --i)
		{
			uint8_t digit = (exponent >> i) & 1;
			ans = (ans * ans) % mod;
			if (digit)
			{
				ans = (ans * base) % mod;
			}
		}

		return ans;
	}


	template <typename Digit>
	struct proth
	{
		Digit N;
		Digit p;
		Digit k;
		Digit g;
		Digit g_inv;
	};

	template <typename Digit, uint32_t log_base, typename Container = std::vector<Digit>>
	proth<Digit> get_fft_proth_prime(const Container& container)
	{
		constexpr Digit one = 1;

		const Digit n = container.size();
		const auto logn = log2(n).first;
		assert(log2(n).second);
		uint32_t idx = logn;
		
		while (proth_primes::proth_list[idx].p < (ONE << (1*log_base)))
		{
			idx++;
		}

		const auto &chosen_proth = proth_primes::proth_list[idx];
		assert(log2(chosen_proth.p).first + 
				(log2(chosen_proth.p).second == false) 
				< 8*sizeof(Digit));
		proth<Digit> ans{static_cast<Digit> (chosen_proth.N), 
						 static_cast<Digit> (chosen_proth.p), 
						 static_cast<Digit> (chosen_proth.k), 
						 static_cast<Digit> (chosen_proth.g), 
						 static_cast<Digit> (chosen_proth.g_inv)};
		
		//adjust k so that proth.n == logn
		if (ans.N > logn)
		{
			auto pow_diff = chosen_proth.N - logn;
			ans.k = ans.k * (one << pow_diff);
			ans.N -= pow_diff;
		}

		assert(ans.N == logn);
		return ans;
	}

	/* @brief				Computer the number theoretic fft of the given vector with
	 * 						the given proth prime. Most of the preconditions associated
	 * 						with it can be avoided by using one of the wrapper functions
	 * 						below.
	 * @precondition		1- The elements of Container should be of type
	 * 						Digit.
	 * @precondition		2- container.size() should be a power of 2.
	 * @precondition		Let p be the proth_prime corresponding to 
	 * 						container.size(), then:
	 * 						3- p should fit in a digit.
	 * @precondition		4- DoubleDigit should have twice as many bits 
	 * 						as Digit
	 * @precondition		5- No element in container should be >= than
	 * 						2^log_base
	 */
	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base,
			 typename Container = std::vector<Digit>, bool is_inverse = false>
	Digit in_place_discrete_fft(Container &container, const proth<Digit>& proth)
	{
		in_place_bit_reverse_swap(container);
		
		const Digit n = container.size();
		const auto logn = log2(n).first;
		assert(log2(n).second);
		
		const Digit g = is_inverse ? proth.g_inv : proth.g;
		const Digit p = proth.p;
		const Digit k = proth.k;
		std::vector<Digit> w_fft_vector;
		w_fft_vector.reserve(logn + 1);
		for (auto s = 1u; s<= logn; s++)
		{
			w_fft_vector[s] =  modular_exp<Digit, DoubleDigit>(g, ((p-1)) >> s, p);
		}

		for(auto s = 1u; s <= logn; s++)
		{
			const Digit one = 1;
			const Digit fft_size = (one << s);
			const Digit w_fft = w_fft_vector[s];

			for(Digit leaf_base = 0; leaf_base < n; leaf_base += fft_size)
			{
				DoubleDigit w = 1;

				for(Digit fft_it = 0; fft_it < fft_size/2; fft_it++)
				{
					DoubleDigit t = container[leaf_base + fft_it + fft_size/2];
					t = (t * w) % p;
					DoubleDigit u = container[leaf_base + fft_it];
					container[leaf_base + fft_it] = (u + t) % p;
					container[leaf_base + fft_it + fft_size/2] = ((u + p) - t) % p;
					w = (w * w_fft) % p;
				}
			}
		}
		
		if(is_inverse)
		{
			DoubleDigit size_inverse = k;
			size_inverse = (size_inverse * (p-1)) % p;
			for(Digit &x : container)
			{
				DoubleDigit dx = x;
				x = (size_inverse * dx) % p;
			}
		}

		return p;
	}

	/* @brief	Cmputes the inverse fft. Same preconditions from the normal 
	 * 			transformation apply.
	 */
	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base,
			 typename Container = std::vector<Digit>>
	Digit in_place_discrete_inverse_fft(Container &container, const proth<Digit>& proth)
	{
		return in_place_discrete_fft<Digit, DoubleDigit, log_base, Container, true>(container, proth);
	}

	/* @brief	Helper functions that abstract the choice of the proth prime used.
	 */
	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base,
			 typename Container = std::vector<Digit>, bool is_inverse = false>
	Digit in_place_discrete_fft(Container &container)
	{
		const auto proth = get_fft_proth_prime<Digit, log_base, Container>(container);
		return in_place_discrete_fft<Digit, DoubleDigit, log_base, Container, false>(container, proth);
	}

	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base,
			 typename Container = std::vector<Digit>>
	Digit in_place_discrete_inverse_fft(Container &container)
	{
		const auto proth = get_fft_proth_prime<Digit, log_base, Container>(container);
		return in_place_discrete_fft<Digit, DoubleDigit, log_base, Container, true>(container, proth);
	}
}

#endif
