#ifndef __BIGNUM_H__
#define __BIGNUM_H__

#include <cstdint>
#include <vector>
#include <string>
#include <cassert>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <iostream>

namespace
{
	template<typename T>
	struct bn_digits;

	template<>
	struct bn_digits<uint32_t> 
	{
		using digit = uint32_t;
		using ddigit = uint64_t;
	};

	template<>
	struct bn_digits<uint64_t> 
	{
		using digit = uint64_t;
		using ddigit = unsigned __int128;
	};
}

namespace bn
{

	template <typename T, uint32_t base>
	class bignum;
	template <typename T, uint32_t base>
	bignum<T, base> bn_mult(const bignum<T, base>& a_, const bignum<T, base>& b_);


	/* @brief		Class intended to represent arbitrary length numbers.
	 * 				It is mostly intended to be used along the arithmethic library
	 * 				provided.
	 * 				Two template instantiations well suited for such applications 
	 * 				are provided in the end of this file. See bn32 and bn64 types.
	 * @remarks		This class is meant to be instantiated by the static method 
	 * 				from_string(), but other constructors are available.
	 * @remarks		base	represents the number os bits used by each digit.
	 */
	template <typename T, uint32_t base>
	class bignum
	{
		public:
			//Type stored by the container
			using digit = typename bn_digits<T>::digit;
			//Type used when operations might overflow
			using ddigit = typename bn_digits<T>::ddigit;
			//The type of the container used to store digits
			using digit_container = typename std::vector<digit>;
			//The numbers of bits used of the type "digit" for each digit of the number.
			static constexpr auto BASE = base;
			static constexpr auto EXCESS = 8*sizeof(T) - base;

			template <typename T1, uint32_t base1>
			friend  bignum<T1, base1> bn_mult(const bignum<T1, base1>&, const bignum<T1, base1>&);

			/*	@brief 		Builds a bignum by simply copying the given container.
			 *				Note that the lower "base" bits are the only bits allowed to
			 *				be non zero in each digit of the container.
			 */
			bignum(const digit_container &digits) : digits_(digits) {}
			bignum(digit_container &&digits) : digits_(std::move(digits)) {}

			const digit_container& digits() const
			{
				return digits_;
			}

			/*	@brief		Returns the number of digits (in base "base") in the number 
			 *				represented by this object. Leading zeros are ignored.
			 */
			std::size_t num_digits() const
			{
				std::size_t leading_zeros = 0u;
				for (auto i = digits_.crbegin(); i!= digits_.crend();
						i++)
				{
					if (*i != 0)
					{
						break;
					}
					leading_zeros++;
				}
				return std::max(std::size_t{1}, digits_.size() - leading_zeros);
			}

			/*	@brief		Adds leading zeros until the underlying container has "size"
			 *				elements. If size is smaller than the current number of elements,
			 *				no operation is performed.
			 */
			void pad_to(std::size_t size)
			{
				if (size < digits_.size())
				{
					return;
				}
				digits_.resize(size, 0);
			}

			/*	@brief		Returns the number of digits in base "base" required to
			 *				represent "num_hex_digits" hexadecimal digits.
			 */
			static std::size_t digits_needed_from_hex(std::size_t num_hex_digits)
			{
				return ((4 * num_hex_digits) + (base - 1)) / base;
			}

			/*	@brief			Converts number in hexadecimal notation to a bignum.
			 *	@precondition	The string should not begin with "0x".
			 *	@precondition	The string should contain characters in [a-f0-9]
			 */
			static bignum from_string(const std::string &hex_digits)
			{
				auto it = std::find_if(hex_digits.cbegin(), hex_digits.cend(), 
						[](char c)
						{
							return c != '0';
						}
				);
				auto distance = it - hex_digits.cbegin();
				auto beg = hex_digits.rbegin();
				auto end = hex_digits.rend() - distance;

				auto num_digits = digits_needed_from_hex(end - beg);
				std::vector<digit> digits_;
				digits_.reserve(num_digits);

				if(beg == end)
				{
					digits_.push_back(0);
				}
				
				std::vector<bool> bitset(num_digits * base);
				std::size_t count = 0;
				for (; beg != end; beg++)
				{
					auto hex_digit = *beg;
					digit value = 0;
					if (hex_digit >= 'a' && hex_digit <= 'f')
					{
						value = 10 + hex_digit - 'a';	
					}
					else if (hex_digit >= '0' && hex_digit <= '9')
					{
						value = hex_digit - '0';
					}
					else
					{
						bool invalid_hex_digit = false;
						assert(invalid_hex_digit);
					}

					bitset[4 * count + 0] = value & 1u;
					bitset[4 * count + 1] = value & 2u;
					bitset[4 * count + 2] = value & 4u;
					bitset[4 * count + 3] = value & 8u;
					count++;
				}
				for (count = 0; count < num_digits; count ++)
				{
					digit x = 0;
					for (uint32_t i = 0; i < base; i++)
					{
						x |= static_cast<digit>(bitset[count * base + i]) << i;
					}
					digits_.push_back(x);
				}

				return bignum(std::move(digits_));
			}

			/* @brief		Converts this bignum into a hexadecimal number.
			 */
			std::string to_string() const
			{				
				auto beg = digits_.crbegin();
				auto end = digits_.crend();
				beg = std::find_if(beg, end,
						[](digit d)
						{
							return d != 0;
						}
				);

				if (beg == end)
				{
					return "0";
				}

				std::vector<bool> bitset((end - beg) * base);
				std::size_t count = 0;
				for (; beg != end; ++beg)
				{
					for (uint32_t i = 0; i < base; i++)
					{
						bitset[count * base + i] = (*beg) & (1 << (BASE - 1 - i));
					}
					count++;
				}
				
				std::stringstream stream;
				int shift_ammount = (3 + bitset.size()) % 4;
				uint8_t hex = 0;
				for (auto bit : bitset)
				{
					hex |= static_cast<uint8_t>(bit) << (shift_ammount);
					shift_ammount--;
					if (shift_ammount < 0)
					{
						shift_ammount = 3;
						stream << std::hex << hex * 1u;
						hex = 0;
					}
				}
				if (shift_ammount != 3)
				{
					stream << std::hex << hex * 1u;
				}

				auto string = stream.str();
				return std::string(string.begin() + string.find_first_not_of('0'), string.end());
			}
		
		private:
			std::vector<digit> digits_;
	};

	using bn32 = bignum<uint32_t, 2>;
	using bn64 = bignum<uint64_t, 10>;
}

#endif
