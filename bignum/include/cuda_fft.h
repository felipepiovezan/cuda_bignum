#ifndef _CUDA_FFT_H
#define _CUDA_FFT_H

#include <cassert>
#include <cstdint>
#include <utility>
#include "proth_primes.hpp"
#include <vector>
#include "utils.hpp"
#include <iostream>
#include <stdio.h>


namespace
{
	void assert_and_print(cudaError_t error)
	{
		if (error != cudaError::cudaSuccess)
		{
			std::cout << cudaGetErrorString(error) << std::endl;
			assert(error == cudaError::cudaSuccess);
		}
	}
}

namespace utils
{
	/*
	 * @brief		Computes base^exponent modulo mod
	 */
	template <typename Digit, typename DoubleDigit = Digit>
	__device__ Digit cuda_modular_exp(Digit base, Digit exponent, Digit mod)
	{
		DoubleDigit ans = 1;
		for(int i = sizeof(Digit)*8 - 1 ; i >= 0; --i)
		{
			uint8_t digit = (exponent >> i) & 1;
			ans = (ans * ans) % mod;
			if (digit)
			{
				ans = (ans * base) % mod;
			}
		}

		return ans;
	}

	/* @brief		 Computes two positions of the "s-th" fft iteration.
	 * @precondition Should be called with at least n/2 threads.
	 * @precondition Should be called with one dimensional grids and blocks.
	 */
	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base,
			 bool is_inverse = false>
	__global__ void fft_kernel(Digit *container, Digit w_fft, 
			uint32_t logn, uint32_t n, proth<Digit> proth, Digit s)
	{	
		const Digit p = proth.p;
		const Digit th_id = blockIdx.x*blockDim.x + threadIdx.x;

		if (th_id >= n/2)
		{
			return;
		}

		const Digit one = 1;
		const Digit fft_size = (one << s);
		const Digit fft_base = (one << s) * (th_id >> (s-1));
		const Digit fft_it = (th_id & ((one << (s-1)) - 1)); //thread id % 2^(tree_level - 1)
		const Digit u_idx = fft_base + fft_it;
		const Digit t_idx = u_idx + fft_size/2; //fft_base + fft_it + fft_size/2
		const Digit w = cuda_modular_exp<Digit, DoubleDigit>(w_fft, fft_it, p);
			
		DoubleDigit t = container[t_idx];
		t = (t * w) % p;
		DoubleDigit u = container[u_idx];
		container[u_idx] = (u + t) % p;
		container[t_idx] = ((u + p) - t) % p;
	}

	/* @brief		 Computes the inverse step of the fft algorithm for a single
	 * 				 vector position.
	 * @precondition Should be called with at least n threads.
	 * @precondition Should be called with one dimensional grids and blocks.
	 */
	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base>
	__global__ void fft_kernel_inverse_step(Digit *container, 
			uint32_t logn, uint32_t n, proth<Digit> proth)
	{
		const Digit p = proth.p;
		const Digit k = proth.k;
		const Digit th_id = blockIdx.x*blockDim.x + threadIdx.x;
		
		if (th_id >= n)
		{
			return;
		}

		DoubleDigit size_inverse = k;
		size_inverse = (size_inverse * (p-1)) % p;
		DoubleDigit dx = container[th_id];
		container[th_id] = (size_inverse * dx) % p;
	}

	/* @brief				Computer the number theoretic fft of the given vector with
	 * 						the given proth prime. Most of the preconditions associated
	 * 						with it can be avoided by using one of the wrapper functions
	 * 						below.
	 * @precondition		1- The elements of Container should be of type
	 * 						Digit.
	 * @precondition		2- container.size() should be a power of 2.
	 * @precondition		Let p be the proth_prime corresponding to 
	 * 						container.size(), then:
	 * 						3- p should fit in a digit.
	 * @precondition		4- DoubleDigit should have twice as many bits 
	 * 						as Digit
	 * @precondition		5- No element in container should be >= than
	 * 						2^log_base
	 */
	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base,
			 typename Container = std::vector<Digit>, bool is_inverse = false>
	Digit cuda_in_place_discrete_fft(Container &container, const proth<Digit>& proth)
	{
		in_place_bit_reverse_swap(container);
		
		const Digit n = container.size();
		const auto logn = log2(n).first;
		assert(log2(n).second);
		
		const Digit g = is_inverse ? proth.g_inv : proth.g;
		const Digit p = proth.p;
		std::vector<Digit> w_fft_vector;
		w_fft_vector.reserve(logn + 1);
		w_fft_vector.push_back(0);
		for (auto s = 1u; s<= logn; s++)
		{
			w_fft_vector.push_back(modular_exp<Digit, DoubleDigit>(g, ((p-1)) >> s, p));
		}

		Digit *d_container;
		auto err = cudaMalloc(&d_container, container.size() * sizeof(Digit));
		assert(err == cudaError::cudaSuccess);
		err = cudaMemcpy(d_container, container.data(), container.size() * sizeof(Digit), cudaMemcpyHostToDevice);
		assert(err == cudaError::cudaSuccess);
	
		dim3 dim_grid, dim_block;
		dim_grid.x = (n+1023)/1024;
		dim_block.x = 1024;
		for(auto s = 1u; s <= logn; s++)
		{
			fft_kernel<Digit, DoubleDigit, log_base, is_inverse><<<dim_grid, dim_block>>>(d_container, w_fft_vector[s], logn, n, proth, s);
			assert_and_print(cudaPeekAtLastError());
			assert_and_print(cudaDeviceSynchronize());
		}

		if (is_inverse)
		{
			fft_kernel_inverse_step<Digit, DoubleDigit, log_base><<<dim_grid, dim_block>>>(d_container, logn, n, proth);
		}

		err = cudaMemcpy(container.data(), d_container, container.size() * sizeof(Digit), cudaMemcpyDeviceToHost);
		if (err != cudaError::cudaSuccess)
		{
			std::cout << cudaGetErrorString(err) << std::endl;
		}
		assert(err == cudaError::cudaSuccess);

		err = cudaFree(d_container);
		assert(err == cudaError::cudaSuccess);

		return p;
	}

	/* @brief	Cmputes the inverse fft. Same preconditions from the normal 
	 * 			transformation apply.
	 */
	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base,
			 typename Container = std::vector<Digit>>
	Digit cuda_in_place_discrete_inverse_fft(Container &container, const proth<Digit>& proth)
	{
		return cuda_in_place_discrete_fft<Digit, DoubleDigit, log_base, Container, true>(container, proth);
	}

	/* @brief	Helper functions that abstract the choice of the proth prime used.
	 */
	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base,
			 typename Container = std::vector<Digit>, bool is_inverse = false>
	Digit cuda_in_place_discrete_fft(Container &container)
	{
		const auto proth = get_fft_proth_prime<Digit, log_base, Container>(container);
		return cuda_in_place_discrete_fft<Digit, DoubleDigit, log_base, Container, false>(container, proth);
	}

	template <typename Digit, typename DoubleDigit = Digit, uint32_t log_base,
			 typename Container = std::vector<Digit>>
	Digit cuda_in_place_discrete_inverse_fft(Container &container)
	{
		const auto proth = get_fft_proth_prime<Digit, log_base, Container>(container);
		return cuda_in_place_discrete_fft<Digit, DoubleDigit, log_base, Container, true>(container, proth);
	}
}

#endif
