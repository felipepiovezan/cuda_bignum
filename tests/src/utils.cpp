#include "utils.hpp"
#include "gtest/gtest.h"
#include <random>
#include <iostream>
#include <chrono>

TEST(utils, log2works)
{
	using namespace utils;
	ASSERT_EQ(utils::log2(1), std::make_pair(0u, true));
	ASSERT_EQ(utils::log2(2), std::make_pair(1u, true));
	ASSERT_EQ(utils::log2(3), std::make_pair(1u, false));
	ASSERT_EQ(utils::log2(4), std::make_pair(2u, true));
	ASSERT_EQ(utils::log2(5), std::make_pair(2u, false));
	ASSERT_EQ(utils::log2(1<<4), std::make_pair(4u, true));
	ASSERT_EQ(utils::log2((1<<4) + 1), std::make_pair(4u, false));
	uint64_t one = 1;
	ASSERT_EQ(utils::log2((one<<33) + 33), std::make_pair(33u, false));
	ASSERT_EQ(utils::log2((one<<63) + 123123), std::make_pair(63u, false));
}

TEST(utils, rev_works)
{
	using namespace utils;
	ASSERT_EQ(rev(0b0, 1),  0b0);
	ASSERT_EQ(rev(0b0000000, 7),  0b0);
	ASSERT_EQ(rev(0b1, 1),  0b1);
	ASSERT_EQ(rev(0b0001, 4),  0b1000);
	ASSERT_EQ(rev(0b10, 2), 0b01);
	ASSERT_EQ(rev(0b11, 2), 0b11);
	ASSERT_EQ(rev(0b100, 3),0b001);
	ASSERT_EQ(rev(0b010110, 3),0b000011);
	ASSERT_EQ(rev(0b010110, 6),0b011010);
	ASSERT_EQ(rev(0b010110), 0b01101000'00000000'00000000'00000000);
}

TEST(utils, bit_reverse_swap_works_with_small_input)
{
	using namespace utils;
	std::vector<uint32_t> v  = {0, 1, 2, 3, 4, 5, 6, 7};
	std::vector<uint32_t> vs = {0, 4, 2, 6, 1, 5, 3, 7};
	in_place_bit_reverse_swap(v);
	ASSERT_EQ(v, vs);
}

TEST(utils, bit_reverse_swap_works_with_big_vector)
{
	auto pow = 20;
	auto size = (1 << pow);
	using namespace utils;
	std::vector<std::uint32_t> v;
	v.reserve(size);
	for(uint32_t i = 0; i < size; i++)
	{
		v.push_back(i);
	}
	
	in_place_bit_reverse_swap(v);

	for(uint32_t i = 0; i < size; i++)
	{
		ASSERT_EQ(v[i], rev(i, pow)) << "Failed at postion i = " << i;
	}
}

TEST(utils, modular_exponentiation)
{
	using namespace utils;
	ASSERT_EQ(modular_exp(2, 10, 11111), 1024);
	ASSERT_EQ(modular_exp(2, 10, 2), 0);
	ASSERT_EQ(modular_exp(2, 10, 64), 0);
	ASSERT_EQ(modular_exp(2, 10, 17), 4);
	ASSERT_EQ(modular_exp(2, 10, 123), 40);
	ASSERT_EQ(modular_exp(17, 200, 3777), 1756);

	ASSERT_EQ(modular_exp(2u, 10u, 11111u), 1024u);
	ASSERT_EQ(modular_exp(2u, 10u, 2u), 0u);
	ASSERT_EQ(modular_exp(2u, 10u, 64u), 0u);
	ASSERT_EQ(modular_exp(2u, 10u, 17u), 4u);
	ASSERT_EQ(modular_exp(2u, 10u, 123u), 40u);
	ASSERT_EQ(modular_exp(17u, 200u, 3777u), 1756u);

	ASSERT_EQ(modular_exp<uint64_t>(123123123123, 123123123, 2), 1);
	ASSERT_EQ(modular_exp<uint64_t>(12312312312333, 123123123, 2), 1);
	ASSERT_EQ(modular_exp<uint64_t>(12312312312333333333u, 123123123, 2), 1);
	{auto result = modular_exp<uint64_t, unsigned __int128>(12312312312333333333u, 123123123, 123123);
	ASSERT_EQ(result, 17919);}
}

TEST(utils, in_place_discrete_fft_works)
{
	using namespace utils;

	std::mt19937 gen(173732);
	constexpr uint32_t log_base = 10;
	for (uint64_t size = (1<<20); size <= (1<<27); size *= 2)
	{
		std::vector<uint32_t> original_input(size);
		std::vector<uint32_t> input(size);
		for(auto tries = 1;  tries < 2; tries++)
		{
			for(auto &x : original_input)
			{
				x = gen()>>(32-log_base);
			}
			input = original_input;
			
			std::cout << "starting discrete fft of size = 2^" << log2(size).first << "...";
			std::cout.flush();
			auto start = std::chrono::system_clock::now();
			
			in_place_discrete_fft<uint32_t, uint64_t, log_base>(input);
			
			auto end = std::chrono::system_clock::now();
			std::chrono::duration<double> elapsed_time = end-start;
			std::cout << elapsed_time.count() << "s" << std::endl;

			std::cout << "starting inverse discrete fft of size = 2^" << log2(size).first << "...";
			std::cout.flush();
			start = std::chrono::system_clock::now();

			in_place_discrete_inverse_fft<uint32_t, uint64_t, log_base>(input);
			
			end = std::chrono::system_clock::now();
			elapsed_time = end-start;
			std::cout << elapsed_time.count() << "s" << std::endl;
			
			ASSERT_EQ(input, original_input) << "tries = 2^" << log2(size).first;
		}
	}
}
