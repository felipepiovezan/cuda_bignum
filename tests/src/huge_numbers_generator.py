from random import randrange
from math import log2
#LIMIT = 2**1000000000
LIMIT = 2**10000000

with open("input_numbers.h", "w") as f:
    f.write('std::string test_vector[][3] = {\n')
    exp = 2
    count = 1
    while exp < LIMIT:
        print("Generating numbers with 2^{}bits\n".format(log2(log2(exp))))
        count +=1
        x, y = randrange(exp), randrange(exp)
        xy = x*y
        f.write('\t{')
        f.write('"{}", "{}", "{}"'.format(
            str(hex(x))[2:],
            str(hex(y))[2:],
            str(hex(xy))[2:],
        ))
        f.write('}')
        exp = exp * exp
        if exp < LIMIT:
            f.write(',')
        f.write('\n')
    f.write('};\n')

