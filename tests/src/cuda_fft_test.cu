#include "cuda_fft.h"
#include "gtest/gtest.h"
#include <random>
#include <iostream>
#include <chrono>


std::string get_first_diff_element(const std::vector<uint32_t> &a, const std::vector<uint32_t> &b)
{
	for (int i = 0; i < a.size(); i++)
	{
		if (a[i] != b[i])
		{
			std::string ans = "different at pos " + std::to_string(i) + "\n";
			ans += "a = " + std::to_string(a[i]) + "  " ;
			ans += "b = " + std::to_string(b[i]);
			return  ans;
		}
	}

	return "all equal";
}

TEST(CUDA, cuda_in_place_discrete_fft_works)
{
	using namespace utils;

	std::mt19937 gen(173732);
	constexpr uint32_t log_base = 10;
	for (uint64_t size = (1<<20); size <= (1<<27); size *= 2)
	{
		std::vector<uint32_t> original_input(size);
		std::vector<uint32_t> input(size);
		for(auto tries = 1;  tries < 2; tries++)
		{
			for(auto &x : original_input)
			{
				x = gen()>>(32-log_base);
			}
			input = original_input;

			std::cout << "starting discrete fft of size = 2^" << log2(size).first << "...";
			std::cout.flush();
			auto start = std::chrono::system_clock::now();
			
			cuda_in_place_discrete_fft<uint32_t, uint64_t, log_base>(input);
			
			auto end = std::chrono::system_clock::now();
			std::chrono::duration<double> elapsed_time = end-start;
			std::cout << elapsed_time.count() << "s" << std::endl;

			std::cout << "starting inverse discrete fft of size = 2^" << log2(size).first << "...";
			std::cout.flush();
			start = std::chrono::system_clock::now();

			cuda_in_place_discrete_inverse_fft<uint32_t, uint64_t, log_base>(input);

			end = std::chrono::system_clock::now();
			elapsed_time = end-start;
			std::cout << elapsed_time.count() << "s" << std::endl;

			ASSERT_EQ(input, original_input) << "size = 2^" << log2(size).first << " " <<get_first_diff_element(input, original_input);
		}
	}
}
