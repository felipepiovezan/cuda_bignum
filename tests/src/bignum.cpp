#include "bignum.hpp"
#include "utils.hpp"
#include "gtest/gtest.h"
#include <sstream>
#include <random>

TEST(bn_basics, num_digits_from_hex_digits_calculated_correctly)
{
	ASSERT_EQ(bn::bn32::digits_needed_from_hex(0),   (0*4 + bn::bn32::BASE - 1)/ bn::bn32::BASE);
	ASSERT_EQ(bn::bn32::digits_needed_from_hex(1),   (1*4 + bn::bn32::BASE - 1)/ bn::bn32::BASE);
	ASSERT_EQ(bn::bn32::digits_needed_from_hex(2),   (2*4 + bn::bn32::BASE - 1)/ bn::bn32::BASE);
	ASSERT_EQ(bn::bn32::digits_needed_from_hex(3),   (3*4 + bn::bn32::BASE - 1)/ bn::bn32::BASE);
	ASSERT_EQ(bn::bn32::digits_needed_from_hex(4),   (4*4 + bn::bn32::BASE - 1)/ bn::bn32::BASE);
	ASSERT_EQ(bn::bn32::digits_needed_from_hex(5),   (5*4 + bn::bn32::BASE - 1)/ bn::bn32::BASE);
	ASSERT_EQ(bn::bn32::digits_needed_from_hex(8),   (8*4 + bn::bn32::BASE - 1)/ bn::bn32::BASE);
	ASSERT_EQ(bn::bn32::digits_needed_from_hex(100), (100*4 + bn::bn32::BASE - 1)/ bn::bn32::BASE); 
}

/*TODO: see comment on the function. Tests will be disabled until
 * it is addressed.
TEST(bn_basics, num_digits_without_leading_zeros_is_correct)
{
	auto bn32 = bn::bn32::from_string("0001");
	ASSERT_EQ(bn32.num_digits(), 1);
	bn32 = bn::bn32::from_string("00000");
	ASSERT_EQ(bn32.num_digits(), 1);
	bn32 = bn::bn32::from_string("111111111");
	ASSERT_EQ(bn32.num_digits(), bn::bn32::digits_needed_from_hex(9));
}*/

TEST(bn_basics, pad_to_adds_leading_zeros)
{
	auto bn32 = bn::bn32::from_string("111444441");
	auto bn32c = bn32;
	bn32.pad_to(1231);
	ASSERT_EQ(bn32.num_digits(), bn32c.num_digits());
}

TEST(bn_from_to_string, bn_from_string_zeros_and_empty_work_correctly)
{
	auto bn32 = bn::bn32::from_string("");
	bn::bn32::digit_container vector32 = {0};
	ASSERT_EQ(bn32.digits().size(), 1);
	ASSERT_EQ(bn32.digits(), vector32);
	
	bn32 = bn::bn32::from_string("0");
	ASSERT_EQ(bn32.digits(), vector32);
	
	bn32 = bn::bn32::from_string("000");
	vector32 = {0};
	ASSERT_EQ(bn32.digits(), vector32);
}

TEST(bn_from_to_string, bn_to_string_works)
{
	std::mt19937 gen(173732);
	std::stringstream stream;
	for(int i = 0; i < 80; i++)
	{
		stream.str("");
		for(int j = 0; j < 4000; j++)
			stream << std::hex << gen();
		std::string string = stream.str();
		auto bn32 = bn::bn32::from_string(string);
		ASSERT_EQ(bn32.to_string(), string)<< i << "-th iteration";
	}
}
